This program is run using the following command:

python src/main.py data/FRACTALNAME.frac

An example .frac file would be as follows:

type: Julia
cReal: -1
cImag: 0
pixels: 1024
centerX: -0.339230468501458
centerY: 0.417970758224314
axisLength: 0.164938488846612
iterations: 78

Enjoy!

Update:
    As a third argument after python, you can now add a gradient type. 
    There are two currently: 'crazy' and 'grayscale'

    example: python src/main.py data/FRACTALNAME.frac GRADIENT
