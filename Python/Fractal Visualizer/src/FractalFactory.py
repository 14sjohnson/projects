import Mandelbrot
import Mandelbrot7
import Julia

def makeFractal(cfg):
    if cfg.get("type") == "mandelbrot":
        frac = Mandelbrot.Mandelbrot(cfg)
    elif cfg.get("type") == "julia":
        frac = Julia.Julia(cfg)
    elif cfg.get("type") == "mandelbrot7":
        frac = Mandelbrot7.Mandelbrot7(cfg)
    return frac
