#defines a basic Gradient
from Color import Color

class Gradient():

    colors = list()

    def __init__(self):
        raise NotImplementedError("Invalid command")
    
    def gradient(self, start = Color(0, 0, 0), stop = Color(255, 255, 255), steps = 100):
        dRed = (stop.r - start.r) / (steps - 1)
        dGrn = (stop.g - start.g) / (steps - 1)
        dBlu = (stop.b - start.b) / (steps - 1)
        return list(
            map(lambda n: Color((n * dRed) + start.r, (n * dGrn) + start.g, (n * dBlu) + start.b) , range(steps)))
