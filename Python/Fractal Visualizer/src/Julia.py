#!/usr/bin/python3

## Julia Set Visualizer
from Fractal import Fractal

class Julia(Fractal):
    def __init__(self, config):
        self.iterations = config.get("iterations") - 1
        self.z = complex(config.get("creal"), config.get("cimag"))
         
    def count(self, c):
        z = self.z
        for i in range(self.iterations):
            c = c * c + z # Get z1, z2, ...
            if abs(c) > 2:
                return i
        return self.iterations
        
