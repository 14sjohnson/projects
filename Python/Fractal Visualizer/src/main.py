#This is where you start the program

import sys
import Config, ImagePainter
import FractalFactory, GradientFactory


conf = Config.Config()
cfg = conf.dict
fractal = FractalFactory.makeFractal(cfg)
grad = GradientFactory.makeGradient(sys.argv, cfg)
img = ImagePainter.ImagePainter()
img.makePicture(cfg, fractal, grad.colors)
