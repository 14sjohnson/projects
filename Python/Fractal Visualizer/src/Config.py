
import sys

class Config():
    def __init__(self):
        self.dict = dict() 

        '''The goal of this function is to accept the config file
        and take out the whitespace and make it lowercase.
        It then casts the values as a float, int, or str
        depending on what it should be.'''
        with open(sys.argv[1]) as f:
            for line in f:
                words = line.split(':')
                words[0] = (words[0].lower()).strip()
                words[1] = (words[1].lower()).strip()
                
                if '.' in words[1]:
                    self.dict[words[0]] = float(words[1])
                elif words[1].isdigit() or words[1][0] == '-':
                    self.dict[words[0]] = int(words[1])
                else:
                    self.dict[words[0]] = words[1]
