import Grayscale
import Crazy


def makeGradient(arg, cfg):
    if len(arg) > 2:
        a = arg[2].lower()
        if a == "grayscale":
            grad = Grayscale.Grayscale(cfg)
        elif a == "crazy":
            grad = Crazy.Crazy(cfg)
        else:
            return Grayscale.Grayscale(cfg)        
        return grad
    else:
        return Grayscale.Grayscale(cfg)