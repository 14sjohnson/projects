from Color import Color
from Gradient import Gradient

class Grayscale(Gradient):
    def __init__(self, cfg):
        self.colors = self.gradient(Color(0,0,0), Color(255,255,255), cfg.get("iterations"))
