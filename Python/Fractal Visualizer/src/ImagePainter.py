from tkinter import Tk, Canvas, PhotoImage, mainloop
import Gradient, sys
import Julia, Mandelbrot

class ImagePainter():
    def makePicture(self, cfg, fractal, grad):
        """Paint a Fractal image into the TKinter PhotoImage canvas.  
        This function displays a really handy status bar so you can see how far
        along in the process the program is."""

        cenX = cfg.get('centerx')
        cenY = cfg.get('centery')
        axLen = cfg.get('axislength')
        pix = cfg.get('pixels')
        iterations = cfg.get('iterations')
        imgName = cfg.get('imagename')
        if imgName == None:
            imgName = "NoTitleProvided"

        # Figure out how the boundaries of the PhotoImage 
        minimum = (cenX - axLen / 2.0, 
        cenY - axLen / 2.0)
        maximum = (cenX + axLen / 2.0,
        cenY + axLen / 2.0)

        # At this scale, how much length and height on the imaginary plane does one pixel take?
        size = abs(maximum[0] - minimum[0]) / (pix)
        
        window = Tk()
        img = PhotoImage(width=(pix), height=(pix))
        fraction = int(pix / 64)

        for c in range(pix):
            if c % fraction == 0:
                # Update the status bar each time we complete 1/64th of the rows
                dots = c // fraction
                percent = c / pix
                print(f"{imgName} ({pix}x{pix}) {'=' * dots}{'_' * (64 - dots)} {percent:.0%}", end='\r', file=sys.stderr)
            for r in range(pix):
                x = minimum[0] + c * size
                y = minimum[1] + r * size
                color = grad[fractal.count(complex(x, y))]
                img.put(color, (c, r))
        print(f"{imgName} ({(pix)}x{(pix)}) ================================================================ 100%", file=sys.stderr)
        
        # Display the image on the screen
        canvas = Canvas(window, width=(pix), height=(pix), bg=grad[0])
        canvas.pack()
        canvas.create_image(((pix)/2, (pix)/2), image=img, state="normal")# Set up the GUI so that we can paint the fractal image on the screen
        img.write(imgName + ".gif")
        print(f"Wrote picture {imgName}.gif")
        mainloop()
    