#!/usr/bin/python3

from Fractal import Fractal

class Mandelbrot7(Fractal):

    def __init__(self, config):
        self.iterations = config.get("iterations") - 1

    def count(self, c):
        z = complex(0,0)
        for i in range(self.iterations):
            z = z * z * z * z * z * z * z + c # Get z1, z2, ...
            if abs(z) > 2:
                return i
        return self.iterations
    
        