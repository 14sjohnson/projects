from Color import Color
from Gradient import Gradient

class Crazy(Gradient):
    def __init__(self, cfg):
        if cfg.get("iterations") % 2 == 0:
            list1 = self.gradient(Color(255,0,0), Color(0,255,0), int(cfg.get("iterations")/2))
            list2 =self.gradient(Color(0,255,0), Color(0,0,255), int(cfg.get("iterations")/2))
        else:
            list1 = self.gradient(Color(255,0,0), Color(0,255,0), int(cfg.get("iterations")/2 - .5))
            list2 =self.gradient(Color(0,255,0), Color(0,0,255), int(cfg.get("iterations")/2 + .5))

        self.colors = list1 + list2