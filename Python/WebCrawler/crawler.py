import sys
from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup


def crawl(url, depth, maxDepth, visited):
    if depth == maxDepth:
        return

    parsed = urlparse(url)      
    scheme = parsed.scheme      #get the main scheme for the relative urls
    netloc = parsed.netloc
    try:
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "html.parser")     #get urls in webpage
    except:
        return

    for link in soup.find_all('a'):
        href = link.get('href')
        parsed = urlparse(href)
        if parsed.netloc == '':
            href = netloc + href
            parsed = urlparse(href)
        if parsed.scheme == '':
            href = scheme +'://' + href
            parsed = urlparse(href)

        if href in visited:
            pass
        else: 
            visited.add(href)                       
            for x in range(depth):
                print('\t', end = '')
            print(href)
            crawl(href, depth + 1, maxDepth, visited)


if len(sys.argv) == 1:
    quit("Needs a URL")
parsed = urlparse(sys.argv[1])
if parsed.netloc == '':
    quit("Must have a valid URL, needs a netloc")
if parsed.scheme == '':
    quit("Must have a valid URL, needs a scheme")

visited = set()
visited.add('/')

if len(sys.argv) == 3:
    crawl(sys.argv[1], 0, int(sys.argv[2]), visited)
else:
    crawl(sys.argv[1], 0, 3, visited)
