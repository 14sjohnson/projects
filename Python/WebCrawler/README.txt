This is a recursive web crawler. Given a webpage, this program finds each hyperlink,
follows it, finds each hyperlink on the following webpage and repeats this process
however deep it's told. The default is 3 webpages deep.

Run this program using the command line:

python crawler.py WEBSITE #OfPagesDeep

Be sure to include the 'https://'

Example:

python crawler.py https://google.com 2

Using Google at 2 webpages deep took my computer less than a minute.

