#!/usr/bin/env python3
import sys
from Report import Report
rpt = Report()

## TODO: Add your code here.

def data(arg1, arg2): 
        #variables with as 's' in front are for the software industry   
    maxEmploy_fips, maxEstab_fips, largest_wage_fips = '','',''
    sMaxEmploy_fips, sMaxEstab_fips, sLargest_wage_fips = '','',''
    estab, maxEstab = 0,0
    sEstab, sMaxEstab = 0,0
    employ, maxEmploy = 0, 0
    sEmploy, sMaxEmploy = 0, 0
    wages, largest_wage = 0,0
    sWages, sLargest_wage = 0,0
    total = 0
    sTotal = 0
    fips = ''   
    sFips = ''
    uniqueWage = 0
    uniqueEstab = 0
    uniqueEmploy = 0
    ann_wage_dict = dict()
    estab_dict = dict()
    employ_dict = dict()
    sUniqueWage = 0
    sUniqueEstab = 0
    sUniqueEmploy = 0
    sAnn_wage_dict = dict()
    sEstab_dict = dict()
    sEmploy_dict = dict()
    rankingWage = list()
    rankingEstab = list()
    rankingEmploy = list()
    sRankingWage = list()
    sRankingEstab = list()
    sRankingEmploy = list()

    with open(arg1) as f:
        for line in f:    
            words = line.split(',')
            if words[2] == '"10"' and words[1] == '"0"':
                if (words[0][3] == '0' and words[0][4] == '0' and words[0][5] == '0'):
                    pass
                elif words[0][1] == 'C' or words[0][1] == 'U':
                    pass
                else:
                    if int(words[8]) > maxEstab:
                        maxEstab = int(words[8])
                        maxEstab_fips = words[0].replace('"','')
                    if int(words[9]) > maxEmploy:
                        maxEmploy = int(words[9])
                        maxEmploy_fips = words[0].replace('"','')
                    if int(words[10]) > largest_wage:
                        largest_wage = int(words[10])
                        largest_wage_fips = words[0].replace('"','')
                    if fips != words[0]:
                        if words[10] in ann_wage_dict:
                            ann_wage_dict[words[10]] += 1
                        else:
                            ann_wage_dict[words[10]] = 0
                        if words[9] in employ_dict:
                            employ_dict[words[9]] += 1
                        else:
                            employ_dict[words[9]] = 0
                        if words[8] in estab_dict:
                            estab_dict[words[8]] += 1
                        else:
                            estab_dict[words[8]] = 0
                        rankingWage.append((int(words[10].replace('"','')), words[0].replace('"','')))
                        rankingEmploy.append((int(words[9].replace('"','')), words[0].replace('"','')))
                        rankingEstab.append((int(words[8].replace('"','')), words[0].replace('"','')))
                        estab += int(words[8])
                        employ += int(words[9])
                        wages += int(words[10])
                        total += 1
                        fips = words[0]

            if words[2] == '"5112"' and words[1] == '"5"':
                if (words[0][3] == '0' and words[0][4] == '0' and words[0][5] == '0'):
                    pass
                elif words[0][1] == 'C' or words[0][1] == 'U':
                    pass
                else:
                    if int(words[8]) > sMaxEstab:
                        sMaxEstab = int(words[8])
                        sMaxEstab_fips = words[0].replace('"','')
                    if int(words[9]) > sMaxEmploy:
                        sMaxEmploy = int(words[9])
                        sMaxEmploy_fips = words[0].replace('"','')
                    if int(words[10]) > sLargest_wage:
                        sLargest_wage = int(words[10])
                        sLargest_wage_fips = words[0].replace('"','')
                    if sFips != words[0]:
                        if words[10] in sAnn_wage_dict:
                            sAnn_wage_dict[words[10]] += 1
                        else:
                            sAnn_wage_dict[words[10]] = 0
                        if words[9] in sEmploy_dict:
                            sEmploy_dict[words[9]] += 1
                        else:
                            sEmploy_dict[words[9]] = 0
                        if words[8] in sEstab_dict:
                            sEstab_dict[words[8]] += 1
                        else:
                            sEstab_dict[words[8]] = 0
                        sRankingWage.append((int(words[10].replace('"','')), words[0].replace('"','')))
                        sRankingEmploy.append((int(words[9].replace('"','')), words[0].replace('"','')))
                        sRankingEstab.append((int(words[8].replace('"','')), words[0].replace('"','')))

                        sEstab += int(words[8])
                        sEmploy += int(words[9])
                        sWages += int(words[10])
                        sTotal += 1
                        sFips = words[0]

    with open(arg2) as f:
        for line in f:
            words = line.split('"')
            for x in range(0,len(rankingWage)):
                if rankingWage[x][1] == words[1]:
                    rankingWage[x] = ((words[3], int(rankingWage[x][0])))
                if rankingEmploy[x][1] == words[1]:
                    rankingEmploy[x] = ((words[3], int(rankingEmploy[x][0])))
                if rankingEstab[x][1] == words[1]:
                    rankingEstab[x] = ((words[3], int(rankingEstab[x][0])))
            for x in range(0,len(sRankingWage)):
                if sRankingWage[x][1] == words[1]:
                    sRankingWage[x] = ((words[3], int(sRankingWage[x][0])))
                if sRankingEmploy[x][1] == words[1]:
                    sRankingEmploy[x] = ((words[3], int(sRankingEmploy[x][0])))
                if sRankingEstab[x][1] == words[1]:
                    sRankingEstab[x] = ((words[3], int(sRankingEstab[x][0])))

            if words[1] == largest_wage_fips:
                largest_wage_fips = words[3]
            if words[1] ==  maxEstab_fips:
                maxEstab_fips = words[3]
            if words[1] ==  maxEmploy_fips:
                maxEmploy_fips = words[3]   
            if words[1] == sLargest_wage_fips:
                sLargest_wage_fips = words[3]
            if words[1] ==  sMaxEstab_fips:
                sMaxEstab_fips = words[3]
            if words[1] ==  sMaxEmploy_fips:
                sMaxEmploy_fips = words[3]   
                
    rankingWage.sort(key=lambda tup: tup[1])
    rankingEstab.sort(key=lambda tup: tup[1])
    rankingEmploy.sort(key=lambda tup: tup[1])
    rankingWage.reverse()
    rankingEstab.reverse()
    rankingEmploy.reverse()
    sRankingWage.sort(key=lambda tup: tup[1])
    sRankingEstab.sort(key=lambda tup: tup[1])
    sRankingEmploy.sort(key=lambda tup: tup[1])
    sRankingWage.reverse()
    sRankingEstab.reverse()
    sRankingEmploy.reverse()


    for x in range(0,len(rankingWage)):
        if x <=4:
            rpt.all.top_annual_wages.append(rankingWage[x])
        if rankingWage[x][0] == 'Cache County, Utah':
            rpt.all.cache_co_pay_rank = x + 1
    for x in range(0,len(rankingWage)):
        if x <=4:
            rpt.all.top_annual_estab.append(rankingEstab[x])
        if rankingEstab[x][0] == 'Cache County, Utah':
            rpt.all.cache_co_estab_rank = x + 1
    for x in range(0,len(rankingWage)):
        if x <=4:
            rpt.all.top_annual_avg_emplvl.append(rankingEmploy[x])
        if rankingEmploy[x][0] == 'Cache County, Utah':
            rpt.all.cache_co_empl_rank = x + 1
    for x in range(0,len(sRankingWage)):
        if x <=4:
            rpt.soft.top_annual_wages.append(sRankingWage[x])
        if sRankingWage[x][0] == 'Cache County, Utah':
            rpt.soft.cache_co_pay_rank = x + 1
    for x in range(0,len(sRankingWage)):
        if x <=4:
            rpt.soft.top_annual_estab.append(sRankingEstab[x])
        if sRankingEstab[x][0] == 'Cache County, Utah':
            rpt.soft.cache_co_estab_rank = x + 1
    for x in range(0,len(sRankingWage)):
        if x <=4:
            rpt.soft.top_annual_avg_emplvl.append(sRankingEmploy[x])
        if sRankingEmploy[x][0] == 'Cache County, Utah':
            rpt.soft.cache_co_empl_rank = x + 1

    for x in estab_dict:
        if estab_dict[x] !=0:
            uniqueEstab +=1
    for x in ann_wage_dict:
        if ann_wage_dict[x] !=0:
            uniqueWage +=1
    for x in employ_dict:
        if employ_dict[x] !=0:
            uniqueEmploy +=1
    for x in sEstab_dict:
        if sEstab_dict[x] !=0:
            sUniqueEstab +=1
    for x in sAnn_wage_dict:
        if sAnn_wage_dict[x] !=0:
            sUniqueWage +=1
    for x in sEmploy_dict:
        if sEmploy_dict[x] !=0:
            sUniqueEmploy +=1
    

    rpt.all.count = total
    rpt.all.unique_pay = len(ann_wage_dict) - uniqueWage
    rpt.all.distinct_pay = len(ann_wage_dict)
    rpt.all.per_capita_avg_wage = wages/employ
    rpt.all.total_pay = wages
    rpt.all.total_estab = estab
    rpt.all.unique_estab = len(estab_dict)-uniqueEstab
    rpt.all.distinct_estab = len(estab_dict)
    rpt.all.total_empl = employ
    rpt.all.unique_empl = len(employ_dict)-uniqueEmploy
    rpt.all.distinct_empl = len(employ_dict)

    rpt.soft.count = sTotal
    rpt.soft.unique_pay = len(sAnn_wage_dict) - sUniqueWage
    rpt.soft.distinct_pay = len(sAnn_wage_dict)
    rpt.soft.per_capita_avg_wage = sWages/sEmploy
    rpt.soft.total_pay = sWages
    rpt.soft.total_estab = sEstab
    rpt.soft.unique_estab = len(sEstab_dict)-sUniqueEstab
    rpt.soft.distinct_estab = len(sEstab_dict)
    rpt.soft.total_empl = sEmploy
    rpt.soft.unique_empl = len(sEmploy_dict)-sUniqueEmploy
    rpt.soft.distinct_empl = len(sEmploy_dict)

data(f'{sys.argv[1]}/2017.annual.singlefile.csv', f'{sys.argv[1]}/area_titles.csv')


# By the time you submit your work to me, this should be the *only* output your
# entire program produces.
print(rpt)
