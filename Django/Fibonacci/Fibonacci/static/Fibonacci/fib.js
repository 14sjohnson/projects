//window.onload
// Set the document title
document.title = 'TODO: Dynamic Fibonacci Sequence in JavaScript';

//set up variables
var div = document.createElement('div');
var para = document.createElement('p');
var title = document.createElement('div');
var slider = document.createElement('INPUT');
var array = []; //this holds the fibonacci numbers

//div variable
div.setAttribute('class', 'blue shadowed stuff-box');


//slider variable
slider.setAttribute('type', 'range');
slider.setAttribute('class', 'slider');
slider.setAttribute('id', 'rangeinput');
slider.setAttribute('min', '0');
slider.setAttribute('max', '50');
slider.setAttribute('value', '0');
slider.max = 50;
slider.value = 0;
slider.setAttribute('oninput', 'fibSequence()');
slider.oninput = function(){fibSequence(this.value);} //when changed, the slider will call fibSequence

//title variable
title.textContent = "Fibonacci Counter for element number " + slider.value + " and its children";


//initialize para's text
para.textContent = '' + slider.value;


function fibSequence(n){
    para.removeChild(para.firstChild);
    para.textContent = ' ';
    var sum;
    var x;
    for (x=0; x<=n; x++){
        if (x == 0){
            array[x] = 0;
        }
        else if (x == 1){
            array[x] = 1;
        } else {
            sum = array[x-2] + array[x - 1];
            array[x] = sum;
        }
        title.textContent = "Fibonacci Counter for element number " + x + " and its children";
        para.textContent += ' ' + array[x];
    }
};


  //add the shadowbox to the document, then append everything to the shadowbox
  document.querySelector('body').appendChild(div);
  div.appendChild(title);
  div.appendChild(slider);
  div.appendChild(para);
  


