from django.urls import include, path
from django.contrib import admin


urlpatterns = [
    path('Fibonacci/', include('Fibonacci.urls')),
    path('admin/', admin.site.urls),
]