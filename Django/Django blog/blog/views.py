from django.shortcuts import render
import datetime
from django.utils import timezone

from django.http import HttpResponse
from django.template import Context, loader
from django.shortcuts import render_to_response



now = datetime.datetime.now()
def index(request):
    return (render_to_response('blog/index.html',{'time': now,}))
def bio(request):    
    return (render_to_response('blog/bio.html',{'time': now,}))
def cheatsheet(request):    
    return (render_to_response('blog/cheatsheet.html',{'time': now,}))
