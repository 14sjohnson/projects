from django.urls import include, path
from django.contrib import admin

app_name = 'blog'
urlpatterns = [
    path('blog/', include('blog.urls')),
    path('admin/', admin.site.urls),
]