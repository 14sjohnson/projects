from django.urls import include, path
from django.contrib import admin

app_name = 'GoldPrice'
urlpatterns = [
    path('GoldPrice/', include('GoldPrice.urls')),
    path('admin/', admin.site.urls),
]